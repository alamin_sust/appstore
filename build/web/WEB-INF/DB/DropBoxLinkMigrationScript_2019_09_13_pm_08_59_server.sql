/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  sparkyai
 * Created: Sep 13, 2019
 */

--Dropbox link migration update script
update app set url = 'https://www.dropbox.com/s/kx78uznqqadaksv/A%20Journey%20Towards%20Jesus%28id%20%3D%2063%29.apk?dl=1' where id = 63;
update app set url = 'https://www.dropbox.com/s/m4ln73tcsalnftl/Adam%20and%20Eve%28id%20%3D%2043%29.apk?dl=1' where id = 43;
update app set url = 'https://www.dropbox.com/s/0ye3kraoi0sgj7o/Bible%203D%20AR%20Story%28id%20%3D%2060%29.apk?dl=1' where id = 60;
update app set url = 'https://www.dropbox.com/s/uusbmfp4d2f4i1p/Bible%20ABCs%20for%20Kids%28id%20%3D%2034%29.apk?dl=1' where id = 34;
update app set url = 'https://www.dropbox.com/s/n7coqtzedcjwjj7/Bible%20App%20for%20Kids%28id%20%3D%2018%29.apk?dl=1' where id = 18;
update app set url = 'https://www.dropbox.com/s/zi6kwyiif9bjcs4/Bible%20Coloring%20Book%20Free%28id%20%3D%205%29.apk?dl=1' where id = 5;
update app set url = 'https://www.dropbox.com/s/z3w5ms3gz5t8x2j/Bible%20Coloring%20for%20Kids%28id%20%3D%2037%29.apk?dl=1' where id = 37;
update app set url = 'https://www.dropbox.com/s/evsvtck9olyn6fe/Bible%20Crush%28id%20%3D%2027%29.apk?dl=1' where id = 27;
update app set url = 'https://www.dropbox.com/s/epkij0k3nihhqrl/Bible%20Cube%20Jump%203D%28id%20%3D%2049%29.apk?dl=1' where id = 49;
update app set url = 'https://www.dropbox.com/s/xzfrv991taugkx6/Bible%20Games%20Pauls%20Mission%28id%20%3D%2071%29.apk?dl=1' where id = 71;
update app set url = 'https://www.dropbox.com/s/1ohl2vu5l8crxva/Bible%20Matchers%28id%20%3D%2055%29.apk?dl=1' where id = 55;
update app set url = 'https://www.dropbox.com/s/ezut11sevwmrrh5/Bible%20Memory%20Game%20Children%28id%20%3D%2072%29.apk?dl=1' where id = 72;
update app set url = 'https://www.dropbox.com/s/8a1ja6ounwig6wl/Bobby%20Bot%20Voice%20Assistant%20for%20Kids%20%26%20Parents%20%28Early%20Access%29%28id%20%3D%2083%29.apk?dl=1' where id = 83;
update app set url = 'https://www.dropbox.com/s/ho90rs0rc5n9nux/Canaan%20Advantage%28id%20%3D%2052%29.apk?dl=1' where id = 52;
update app set url = 'https://www.dropbox.com/s/xqdf060c28sx36g/CBN%20Radio%20%E2%80%93%20Christian%20Music%28id%20%3D%2017%29.apk?dl=1' where id = 17;
update app set url = 'https://www.dropbox.com/s/ah0jya8va9g8sva/Christian%20Kid%20songs%20App%28id%20%3D%2077%29.apk?dl=1' where id = 77;
update app set url = 'https://www.dropbox.com/s/zbqbl8qjktm2vm4/Christian%20music%20for%20kids%28id%20%3D%2076%29.apk?dl=1' where id = 76;
update app set url = 'https://www.dropbox.com/s/8pp5kyvef5nh5ji/Christmas%20Coloring%20Book%20Santa%20game%20for%20kids%28id%20%3D%2075%29.apk?dl=1' where id = 75;
update app set url = 'https://www.dropbox.com/s/8p48e3l5x4b4i8a/Coloring%20Book%20Children%27s%20Bible%28id%20%3D%2056%29.apk?dl=1' where id = 56;
update app set url = 'https://www.dropbox.com/s/hsg7miqdq10row8/Daily%20Bible%20Jigsaw%28id%20%3D%2065%29.apk?dl=1' where id = 65;
update app set url = 'https://www.dropbox.com/s/wvhkrhmu6o1dcty/Easter%20Egg%20Coloring%20Game%20For%20Kid%28id%20%3D%2069%29.apk?dl=1' where id = 69;
update app set url = 'https://www.dropbox.com/s/pbcd9btqe1g1dhn/Easter%20Number%20Coloring%20Book%28id%20%3D%2079%29.apk?dl=1' where id = 79;
update app set url = 'https://www.dropbox.com/s/ihumw04wqv6sdr9/God%20for%20Kids%20Bible%20Devotional%28id%20%3D%2038%29.apk?dl=1' where id = 38;
update app set url = 'https://www.dropbox.com/s/6yrvuvx09v13lxu/Guardians%20of%20Ancora%28id%20%3D%2021%29.apk?dl=1' where id = 21;
update app set url = 'https://www.dropbox.com/s/v9x4qv1fcshwocp/Jericho%20Walls%20Shooter%28id%20%3D%2050%29.apk?dl=1' where id = 50;
update app set url = 'https://www.dropbox.com/s/b2vnyvq1vcxja45/Jesus%20Christian%20Theme%28id%20%3D%201%29.apk?dl=1' where id = 1;
update app set url = 'https://www.dropbox.com/s/vl53p9538kcg3iz/Kids%20Picture%20Viewer%28id%20%3D%2082%29.apk?dl=1' where id = 82;
update app set url = 'https://www.dropbox.com/s/j31rgghjfxmjrim/Kids%20Place%20-%20Parental%20Control%28id%20%3D%2080%29.apk?dl=1' where id = 80;
update app set url = 'https://www.dropbox.com/s/uecsdz8h3blov7d/Kids%20Video%20Player%20-%20License%28id%20%3D%2085%29.apk?dl=1' where id = 85;
update app set url = 'https://www.dropbox.com/s/7hl7m5u44zslh25/Lindy%20%26%20Friends%28id%20%3D%2059%29.apk?dl=1' where id = 59;
update app set url = 'https://www.dropbox.com/s/uffsf0yb1v7yd7j/Line%20Up%20Bible%20Hero%28id%20%3D%2053%29.apk?dl=1' where id = 53;
update app set url = 'https://www.dropbox.com/s/l7q4jsnsg5q087f/MightyNoah%20Adventure%20Game%28id%20%3D%2078%29.apk?dl=1' where id = 78;
update app set url = 'https://www.dropbox.com/s/qakx95pdabbvjvo/Moses%20Adventure%28id%20%3D%2051%29.apk?dl=1' where id = 51;
update app set url = 'https://www.dropbox.com/s/c0p9zrftkx3tibi/My%20Place%20With%20Jesus%28id%20%3D%2073%29.apk?dl=1' where id = 73;
update app set url = 'https://www.dropbox.com/s/p281da9bj1kusdp/Noah%27s%20Bible%20Memory%28id%20%3D%2019%29.apk?dl=1' where id = 19;
update app set url = 'https://www.dropbox.com/s/m4v533s9ppty03d/Noahs%20Bunny%20Problem%28id%20%3D%2036%29.apk?dl=1' where id = 36;
update app set url = 'https://www.dropbox.com/s/1z9i1p0cgf59kn0/Noah%27s%20Elephant%20in%20the%20room%28id%20%3D%2035%29.apk?dl=1' where id = 35;
update app set url = 'https://www.dropbox.com/s/cxdefbv49u69j0s/Play%20The%20Bible%20Trivia%20Challenge%28id%20%3D%2067%29.apk?dl=1' where id = 67;
update app set url = 'https://www.dropbox.com/s/ag7q8ryb8gd0gra/Play%20The%20Bible%20Ultimate%20Verses%28id%20%3D%2046%29.apk?dl=1' where id = 46;
update app set url = 'https://www.dropbox.com/s/jagxj897ezsorxc/Praise%20Evader%28id%20%3D%2026%29.apk?dl=1' where id = 26;
update app set url = 'https://www.dropbox.com/s/07oua2foc5za92c/Praise%20Garden%28id%20%3D%2024%29.apk?dl=1' where id = 24;
update app set url = 'https://www.dropbox.com/s/d57q2v3fj6mmhse/Praise%20Hero%28id%20%3D%2025%29.apk?dl=1' where id = 25;
update app set url = 'https://www.dropbox.com/s/a9v78x28ndlotim/Run%20Hero%20Run%28id%20%3D%2070%29.apk?dl=1' where id = 70;
update app set url = 'https://www.dropbox.com/s/1nxyjjgy8hr9o3h/Running%20at%20Samaria%28id%20%3D%2054%29.apk?dl=1' where id = 54;
update app set url = 'https://www.dropbox.com/s/x755miyn73sv6pi/Safe%20Browser%20Parental%20Control%20-%20Blocks%20Adult%20Sites%28id%20%3D%2081%29.apk?dl=1' where id = 81;
update app set url = 'https://www.dropbox.com/s/6wl77vafmiyejwj/Sheep%20Master%20-%20ChristianBible%28id%20%3D%2048%29.apk?dl=1' where id = 48;
update app set url = 'https://www.dropbox.com/s/d2ueibwlrfufz82/Stained%20Glass%28id%20%3D%2057%29.apk?dl=1' where id = 57;
update app set url = 'https://www.dropbox.com/s/7679vzrjqh3tr8r/Super%20Book%20Bible%2C%20Video%20%26%20Games%28id%20%3D%2013%29.apk?dl=1' where id = 13;
--update app set url = 'https://drive.google.com/open?id=1RlxOjAVPd9dAIpWksBG57da6vLGhRqZQ' where id = 13;
update app set url = 'https://www.dropbox.com/s/f7iwoz5lxp27aty/Super%20Book%20Radio%28id%20%3D%2015%29.apk?dl=1' where id = 15;
--update app set url = 'https://drive.google.com/uc?authuser=0&id=1YhYaHgf1GmwTgJZkUVuMIPVlnXGyzm6_&export=download' where id = 15;
update app set url = 'https://www.dropbox.com/s/nzsfnkcjvmmfn74/Superbook%20Bible%20Trivia%20Game%28id%20%3D%2016%29.apk?dl=1' where id = 16;
update app set url = 'https://www.dropbox.com/s/fe4wb32e1kcwa14/Superbook%20Coloring%20Life%20%28AR%29%28id%20%3D%2014%29.apk?dl=1' where id = 14;
update app set url = 'https://www.dropbox.com/s/zqel7hezj5mu7rk/The%20Game%20of%20the%20Bible%28id%20%3D%2062%29.apk?dl=1' where id = 62;
