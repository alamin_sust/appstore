<%-- 
    Document   : home
    Created on : May 18, 2018, 10:51:22 PM
    Author     : md_al
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.appstore.connection.Database"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Bible-Pad Apps Place</title>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!--light box-->
<script src="js/superfish.js"></script>
<script type="text/javascript" src="js/jquery.lightbox.js"></script>
<link rel="stylesheet" type="text/css" href="css/lightbox.css" media="screen">
	<script type="text/javascript">
		$(function() {
			$('.gallery a').lightBox();
		});
   </script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
   <script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
			});
		});
	</script>
        <style>
            .badge1 {
   position:relative;
}
.badge1[data-badge]:after {
   content:attr(data-badge);
   font-size:1.0em;
   background:#16652f;
   color:white;
   width:150px;height:30px;
   text-align:center;
   line-height:18px;
   border-radius:5%;
   box-shadow:0 0 1px #333;
   padding: 5px;
}
            </style>
</head>
<body>
    <%
    Database db = new Database();
    db.connect();

    try{
    %>
   <div class="header">	
    <div class="header-top">
       <div class="wrap"> 
	         <div class="logo">
				<!--<a href="index.html"><img src="images/logo.png" alt=""/></a>-->
                                <h1><img src="img/main-logo.jpeg" style="height:40px"><a href="home.jsp"> Bible-Pad Apps Place</a></h1>
			 </div>
			 <div class="cssmenu">
				<ul id="nav">
					 <li class="current"><a href="#section-1">Home</a></li>
					 <li><a href="#section-2">Exclusive Apps</a></li>
					 <li><a href="#section-3">All Apps</a></li>
				</ul>
		    </div>
		    <div class="clear"></div>
	   </div>
	 </div>
   </div>
   <div class="banner section" id="section-1">
       <div class="wrap"> 		 
           <div class="wmuSlider example1">
			   <div class="wmuSliderWrapper">
                               <article style="position: absolute; width: 100%; height: auto;  opacity: 0;"> <img style="width: 100%; height: auto; " src="img/banner2.jpg" alt=""> </article>
                                   <article style="position: absolute; width: 100%;height: auto; opacity: 0;"> <img style="width: 100%; height: auto; " src="img/banner1.jpg" alt=""> </article>
				</div>
                <a class="wmuSliderPrev">Previous</a><a class="wmuSliderNext">Next</a>
           </div>
            	      <script src="js/jquery.wmuSlider.js"></script> 
					  <script type="text/javascript" src="js/modernizr.custom.min.js"></script> 
						<script>
       						 $('.example1').wmuSlider();         
   						</script> 	           	      
        </div>
    </div>
	<div class="main" id="container">
	   <div class="content-top section">
	       <div class="wrap">
		        <h2>Recommend Apps</h2>
				<p>Our Top Downloaded Apps</p>
                                
                            <div class="section group">
                                
                                <%
                                    Statement st  = db.connection.createStatement();
                                    String q = "select * from app where is_featured=1 and is_featured!=-1";
                                    ResultSet rs = st.executeQuery(q);
                                    int i=0;
                                    while(rs.next()) {
                                        if(i%3==0&&i>0) {
                                %>
                                <div class="clear"></div>
			   </div>
                                <br>
                                <div class="section group">
                                <%}%>
                                <div class="col_1_of_3 span_1_of_3">
                                    <img src="img/<%=rs.getString("id")%>.jpg" style="height: auto; width: 70%; " alt="" onclick="document.location.href='<%=rs.getString("url")%>'"/>
                                    <div class="desc">
                                                <p><%if(rs.getString("type")!=null && rs.getString("type").equals("free")){%>
                                                    <span class="badge1" data-badge="free"></span>
                                                <%}%>
                                                <%if(rs.getString("url")!=null && !rs.getString("url").startsWith("https://play.google.com/")){%>
                                                    <span class="badge1" data-badge="directly download"></span>
                                                <%}%></p>
						<h4><a href="<%=rs.getString("url")%>"><%=rs.getString("name")%><%--<%="(id = "+rs.getString("id")+")"%>--%></a></h4>
						<p><%=rs.getString("type")%></p>
						<p><%=rs.getString("category")%></p>
                                                <p style="padding-right: 50px;"><%=rs.getString("details")%></p>
                                                <div class="social-icons">	
                                                <ul>	
                                                    <li><button class="btn btn-success" onclick="document.location.href='<%=rs.getString("url")%>'" value="Download">Download</button></li>      	
					        </ul>
		    			</div>
                                    </div>
                                </div>
                                    <%i++;}%>
				<div class="clear"></div>
			   </div>
		    </div>
		 </div>
                                
                                <div class="content-top section" id="section-2">
	       <div class="wrap">
		        <h2>Exclusive Apps</h2>
                        <p>Special Free version may request password to download from dropbox <br><i>(Please send us a email with your purchased receipted after you purchased Bible-Pad Tablet, we will send you the password)</i></p>
                            <div class="section group">
                                
                                <%
                                    Statement st3  = db.connection.createStatement();
                                    String q3 = "select * from app where is_featured=2 and is_featured!=-1";
                                    ResultSet rs3 = st3.executeQuery(q3);
                                    i=0;
                                    while(rs3.next()) {
                                        if(i%3==0&&i>0) {
                                %>
                                <div class="clear"></div>
			   </div>
                                <br>
                                <div class="section group">
                                <%}%>
                                <div class="col_1_of_3 span_1_of_3">
                                    <img src="img/<%=rs3.getString("id")%>.jpg" style="height: auto; width: 70%; " alt="" onclick="document.location.href='<%=rs3.getString("url")%>'"/>
                                    <div class="desc">
                                                <p><%if(rs3.getString("type")!=null && rs3.getString("type").equals("free")){%>
                                                    <span class="badge1" data-badge="free"></span>
                                                <%}%>
                                                <%if(rs3.getString("url")!=null && !rs3.getString("url").startsWith("https://play.google.com/")){%>
                                                    <span class="badge1" data-badge="directly download"></span>
                                                <%}%></p>
						<h4><a href="<%=rs3.getString("url")%>"><%=rs3.getString("name")%><%--<%="(id = "+rs3.getString("id")+")"%>--%></a></h4>
						<p><%=rs3.getString("type")%></p>
						<p><%=rs3.getString("category")%></p>
                                                <p style="padding-right: 50px;"><%=rs3.getString("details")%></p>
                                                <div class="social-icons">	
                                                <ul>	
                                                    <li><button class="btn btn-success" onclick="document.location.href='<%=rs3.getString("url")%>'" value="Download">Download</button></li>      	
					        </ul>
		    			</div>
                                    </div>
                                </div>
                                    <%i++;}%>
				<div class="clear"></div>
			   </div>
		    </div>
		 </div>
	   <div class="content-middle section" id="section-3">
		  <div class="wrap">
				 <h2>All Apps</h2>
				 <p>Our Collection</p>
		   <div class="container">
			<ul id="filters" class="clearfix">
                            <li><span class="filter <%if(request.getParameter("filter")==null || request.getParameter("filter").equals("all")){%>active<%}%>" onclick="document.location.href='home.jsp?filter=all'">All</span></li>
				<li><span class="filter <%if(request.getParameter("filter")!=null && request.getParameter("filter").equals("free")){%>active<%}%>" onclick="document.location.href='home.jsp?filter=free'">free</span></li>
				<li><span class="filter <%if(request.getParameter("filter")!=null && request.getParameter("filter").equals("in-app-purchase/adds")){%>active<%}%>" onclick="document.location.href='home.jsp?filter=in-app-purchase/adds'">in-app-purchase/adds</span></li>
			</ul>
			<div class="clear"></div>
		    <div id="portfoliolist" style="     " class="">
		      <div class="top-box">
                                <%
                                    
                              Statement st2  = db.connection.createStatement();
                                    String q2= "select * from app where is_featured=0 and is_featured!=-1";
                                    if(request.getParameter("filter")!=null &&!request.getParameter("filter").equals("all")) {
                                        q2+=" and type='"+request.getParameter("filter")+"'";
                                    }
                                    ResultSet rs2 = st2.executeQuery(q2);
                              while(rs2.next()) {%>
				<div class="portfolio logo1 mix_all" style=" height: 600px">
					<div class="portfolio-wrapper">	
						<div class="gallery">			
                                                    <img style="height: auto;width: 70%; padding: 10%;" src="img/<%=rs2.getString("id")%>.jpg" onclick="document.location.href='<%=rs2.getString("url")%>'" alt="Image" style="height: 250px; top: 0px;">
						</div>
					</div>
                                            <div class="text" onclick="document.location.href='<%=rs2.getString("url")%>'" onfocus="cursor:pointer">
                                                <p><%if(rs2.getString("type")!=null && rs2.getString("type").equals("free")){%>
                                                    <span class="badge1" data-badge="free"></span>
                                                <%}%>
                                                <%if(rs2.getString("url")!=null && !rs2.getString("url").startsWith("https://play.google.com/")){%>
                                                    <span class="badge1" data-badge="directly download"></span>
                                                <%}%></p>
                                            <h5 ><%=rs2.getString("name")%><%--<%="(id = "+rs2.getString("id")+")"%>--%></h5>
					   <p><%=rs2.getString("type")%></p>
                                            <p><%=rs2.getString("category")%></p>
                                            <p><%=rs2.getString("details")%></p>
                                           
                                           <a href="<%=rs2.getString("url")%>" style="color: green">download</a>
					</div>
				</div>
                                <%}%>
				<div class="clear"></div>
			</div>
		</div>
	<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
	<script type="text/javascript">
	$(function () {
		
		var filterList = {
		
			init: function () {
			
				// MixItUp plugin
				// http://mixitup.io
				$('#portfoliolist').mixitup({
					targetSelector: '.portfolio',
					filterSelector: '.filter',
					effects: ['fade'],
					easing: 'snap',
					// call the hover effect
					onMixEnd: filterList.hoverEffect()
				});				
			
			},
			
			hoverEffect: function () {
			
				// Simple parallax effect
				$('#portfoliolist .portfolio').hover(
					function () {
						$(this).find('.label').stop().animate({bottom: 0}, 200, 'easeOutQuad');
						$(this).find('img').stop().animate({top: -30}, 500, 'easeOutQuad');				
					},
					function () {
						$(this).find('.label').stop().animate({bottom: -40}, 200, 'easeInQuad');
						$(this).find('img').stop().animate({top: 0}, 300, 'easeOutQuad');								
					}		
				);				
			
			}

		};
		
		// Run the show!
		filterList.init();
		
		
	});	
	</script>
	  </div>
	 </div>
    </div>
		
	</div>
    <div class="footer section" id="section-5">
<!--       <div class="footer-top">
     	  <div class="wrap">
     		<h2>Contact</h2>
			<p>Tell Us What You Want to Tell</p>
			<div class="section group">
			  <div class="col span_2_of_3">
				  <div class="contact-form">
				  	  <form method="post" action="contact-post.html">
					    	<div>
					    		<input type="text" class="textbox" value="Name (Required)" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}">
						    	
						    </div>
						    <div>
						    	<input type="text" class="textbox" value="Email (Required)" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}">
						    </div>
						    <div>
						     	<input type="text" class="textbox" value="Subject" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Subject';}">
						    </div>
						    <div>
						    	<textarea value="Message:" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">Describe Your project in detail...</textarea>
						    </div>
						    <button class="btn btn-8 btn-8c">Submit</button>
					    </form>
				  </div>
  				</div>
				<div class="col span_1_of_3">
					<div class="company_address">
				     	<h5>Address:</h5>
				     	<p>NY, USA</p>
						<ul class="list3">
							<li>
								<img src="images/location.png" alt=""/>
								<div class="extra-wrap">
								  <p>Bible AppStore</p>
								</div>
							</li>
							<li>
								<img src="images/phone.png" alt=""/>
								<div class="extra-wrap">
									<p>+1 800 258 2598</p>
								</div>
							</li>
							<li>
								<img src="images/mail.png" alt=""/>
								<div class="extra-wrap">
									<p>info(at)appstore.com</p>
								</div>
							</li>
						</ul>
				   </div>
				 </div>
				 <div class="clear"></div>
			  </div>
     	</div>
     </div>-->
     <div class="footer-bottom">
         <div class="wrap">
             
                    <ul style="text-align: center; color: gray">
                        <li style="font-size: large; color: white"><strong>Third Party Copyrights & Trademarks</strong></li>
                    <li>Andriod and Google Play are the trademark of Google Inc.</li>
                    <li>All third party trademarks and Apps are the property of their respective owners.</li> 
                </ul>
             
         </div>
       <div class="wrap">
        <div class="copy">
            
		    <p class="copy">© 2013 Designed by  <a href="http://w3layouts.com" target="_blank">w3layouts</a> </p>
	    </div>
	    <div class="social-footer">	
			<ul>
                            <li><a href="about.jsp" style="color:white;">About this Apps Place</a></li>
                            <li> &nbsp;<span style="color:white"> | </span>&nbsp; </li>
                            <li><a href="#" style="color:white;">Back to Bible-Pad Website</a></li>
		        	
		   </ul>
	    </div>
	    <div class="clear"></div>
	    <script type="text/javascript">
			$(document).ready(function() {
			
				var defaults = {
		  			containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
		 		};
				
				
				$().UItoTop({ easingType: 'easeOutQuart' });
				
			});
		</script>
           <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
	  </div>
	</div>
	<script src="js/jquery.scrollTo.js"></script>
	<script src="js/jquery.nav.js"></script>
	<script>
	$(document).ready(function() {
		$('#nav').onePageNav({
			begin: function() {
			console.log('start')
			},
			end: function() {
			console.log('stop')
			}
		});
	});
	</script>
</div>
<%
} catch(Exception e) {
    System.out.println(e);
} finally {
    db.close();
}

%>
</body>
</html>

    	
    	
            