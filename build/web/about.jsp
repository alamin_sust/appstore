<%-- 
    Document   : home
    Created on : May 18, 2018, 10:51:22 PM
    Author     : md_al
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.appstore.connection.Database"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Bible-Pad Apps Place</title>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!--light box-->
<script src="js/superfish.js"></script>
<script type="text/javascript" src="js/jquery.lightbox.js"></script>
<link rel="stylesheet" type="text/css" href="css/lightbox.css" media="screen">
	<script type="text/javascript">
		$(function() {
			$('.gallery a').lightBox();
		});
   </script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
   <script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
			});
		});
	</script>
</head>
<body>
    <%
    Database db = new Database();
    db.connect();

    try{
    %>
   <div class="header">	
    <div class="header-top">
       <div class="wrap"> 
	         <div class="logo">
				<!--<a href="index.html"><img src="images/logo.png" alt=""/></a>-->
                                <h1><img src="img/main-logo.jpeg" style="height:40px"><a href="home.jsp"> Bible-Pad Apps Place</a></h1>
			 </div>
			 <div class="cssmenu">
				<ul >
					 <li><a href="home.jsp">Home</a></li>
				</ul>
		    </div>
		    <div class="clear"></div>
	   </div>
	 </div>
   </div>
	<div class="main" id="container">
	   <div class="content-top section">
	       <div class="wrap">
                   <br><br><br>
		        <h2>About Us</h2>
                        <p>This Apps Place combined different kind of Games and Apps that related to Christian and Bible.</p>

                        <p>The aims is letting our users to enhance and customizing more games or tools after they've whose purchased our Christian Kid Tablet .</p>
                            <p>We have selected these best Bible apps and games for you and your kid.</p>

                        <p>You can simple click on any of Apps icons and download it via Google Play directly for most of the apps.</p>
                            <p>There are some exclusive games & apps that we created in-house, you can also click on these apps icons and download via Dropbox directly.</p>

                        <p>Thanks for choosing Bible-Pad, the first Christian Digital Bible device that build for your Kids. We wish this devices will encourage your kid to enjoy it, return again and again with full of Bible fun...</p>
                                
                            
		    </div>
		 </div>
                                
                                
	   
		
	</div>
    <div class="footer section" id="section-5">
<!--       <div class="footer-top">
     	  <div class="wrap">
     		<h2>Contact</h2>
			<p>Tell Us What You Want to Tell</p>
			<div class="section group">
			  <div class="col span_2_of_3">
				  <div class="contact-form">
				  	  <form method="post" action="contact-post.html">
					    	<div>
					    		<input type="text" class="textbox" value="Name (Required)" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}">
						    	
						    </div>
						    <div>
						    	<input type="text" class="textbox" value="Email (Required)" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}">
						    </div>
						    <div>
						     	<input type="text" class="textbox" value="Subject" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Subject';}">
						    </div>
						    <div>
						    	<textarea value="Message:" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">Describe Your project in detail...</textarea>
						    </div>
						    <button class="btn btn-8 btn-8c">Submit</button>
					    </form>
				  </div>
  				</div>
				<div class="col span_1_of_3">
					<div class="company_address">
				     	<h5>Address:</h5>
				     	<p>NY, USA</p>
						<ul class="list3">
							<li>
								<img src="images/location.png" alt=""/>
								<div class="extra-wrap">
								  <p>Bible AppStore</p>
								</div>
							</li>
							<li>
								<img src="images/phone.png" alt=""/>
								<div class="extra-wrap">
									<p>+1 800 258 2598</p>
								</div>
							</li>
							<li>
								<img src="images/mail.png" alt=""/>
								<div class="extra-wrap">
									<p>info(at)appstore.com</p>
								</div>
							</li>
						</ul>
				   </div>
				 </div>
				 <div class="clear"></div>
			  </div>
     	</div>
     </div>-->
     <div class="footer-bottom">
         <div class="wrap">
             
                    <ul style="text-align: center; color: gray">
                        <li style="font-size: large; color: white"><strong>Third Party Copyrights & Trademarks</strong></li>
                        
                    <li>Andriod and Google Play are the trademark of Google Inc.</li>
                    <li>All third party trademarks and Apps are the property of their respective owners.</li> 
                </ul>
             <br>
             <ul style="text-align: center; color: gray">
                 <li style="font-size: small; color: gray">
                     Bible-Pad Phoenix LLC is not liable for any infringement of copyright arising out of materials posted on or transmitted through the site, or items advertised on the site, by end users or any other third parties. If you believe that your rights in copyright are being violated by any materials posted on or transmitted through the site, please contact us promptly so that we may investigate the situation and, if appropriate, block or remove the offending materials.
                 </li>
             </ul>
              
             
         </div>
         <br>
       <div class="wrap">
        <div class="copy">
		    <p class="copy">© 2013 Designed by  <a href="http://w3layouts.com" target="_blank">w3layouts</a> </p>
	    </div>
	    <div class="social-footer">	
			<ul>
                            <li><a href="about.jsp" style="color:white;">About this Apps Place</a></li>
                            <li> &nbsp;<span style="color:white"> | </span>&nbsp; </li>
                            <li><a href="#" style="color:white;">Back to Bible-Pad Website</a></li>
		        	
		   </ul>
	    </div>
	    <div class="clear"></div>
	    <script type="text/javascript">
			$(document).ready(function() {
			
				var defaults = {
		  			containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
		 		};
				
				
				$().UItoTop({ easingType: 'easeOutQuart' });
				
			});
		</script>
           <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
	  </div>
	</div>
	<script src="js/jquery.scrollTo.js"></script>
	<script src="js/jquery.nav.js"></script>
	<script>
	$(document).ready(function() {
		$('#nav').onePageNav({
			begin: function() {
			console.log('start')
			},
			end: function() {
			console.log('stop')
			}
		});
	});
	</script>
</div>
<%
} catch(Exception e) {
    System.out.println(e);
} finally {
    db.close();
}

%>
</body>
</html>

    	
    	
            