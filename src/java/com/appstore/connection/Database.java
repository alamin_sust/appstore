/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appstore.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Pranab
 */
public class Database {

    public Connection connection = null;

    private final String USER_NAME = "root";
    private final String PASSWORD = "2011331055";
    private final String DB_URL = "jdbc:mysql://localhost:3306/appstoredb?zeroDateTimeBehavior=convertToNull";
    private final String DRIVER_NAME = "com.mysql.jdbc.Driver";

    public boolean connect() {
        try {
            Class.forName(DRIVER_NAME);
            connection = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void close() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}
